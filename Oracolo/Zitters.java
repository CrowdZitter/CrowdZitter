import java.util.*;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.*;

public class Zitters extends HashMap<String,Zitter> {
    public Zitter put(Zitter z) {
        return super.put(z.getName(),z);
    };

    /**
     * "assorbe" un messaggio MQTT:
     * - spacchetta nome nodo
     * - vede se nodo esiste già
     * - se non esiste lo crea e lo aggiunge
     * - in ogni caso aggiorna attributi del nodo
     * - restituisce lo Zitter "affected"
     *
     * ES:
     * - arriva CrowdZitter/567/segnalazione/canaleCorrente	5
     * - spacchettata in tokens "/"
     */
    public Zitter absorbMessage(String topic, MqttMessage message) {
        removeOldZitters(5*1000);

        //System.err.println("===");
        StringTokenizer st=new StringTokenizer(topic,Globals.TOPIC_DELIMITER);


        // CrowdZitter
        if(!st.hasMoreTokens())
            return null; // vuoto, esce
        else if(!st.nextToken().equals(Globals.ROOT_TOPIC))
            return null; // non comincia con topic giusto, esce

        // name
        String name=st.nextToken();
        if(name.length()==0)
            return null; // nome vuoto, esce

        // segnalazione
        if(!st.hasMoreTokens())
            return null; // vuoto, esce
        else if(!st.nextToken().equals(Globals.SIGNAL_SUBTOPIC))
            return null; // non prosegue con topic giusto, esce


        // tipo messaggio
        if(!st.hasMoreTokens())
            return null; // non c'è avanzo, esce

        Zitter z=getOrCreate(name);
        z.absorbMessage(st.nextToken(),message); // deve essere l'ultimo TODO:verifica?
        return z;
    };

    /**
     * se non trova elemento lo crea (aggiungendolo)
     */
    public Zitter getOrCreate(String name) {
        Zitter z=get(name); // prova a vedere se c'è
        if(z!=null) return z; // ok, c'è

        z=new Zitter(name); // se non c'è lo crea usando il nome
        put(z); // e lo inserisce usando nome come chiave
        return z;
    };


    /** consulta tutti i proxy per sapere se sul canale channel c'è pub
     */
    public long howManyAdvertise(int channel) {
        /* AVANZO
        int howmany=0;
        forEach((k,v)-> {
            System.out.print("lambda: ");
            System.out.print(k);
            System.out.print(",");
            System.out.println(v);
            howmany+=(v.advertOn(channel)==ThreeStateAnswer.YES ? 0 : 1);
        });
        */

        return this.values().stream().filter(z->z.advertOn(channel) == ThreeStateAnswer.YES).count();
    }

    public long howManyOnChan(int channel) {
        return this.values().stream().filter(z->z.getChannel() == channel).count();
    }

    public double ratioAdvertiseOnChannel(int channel) {
        return howManyAdvertise(channel)/((double)howManyOnChan(channel));
    }

    private void removeOldZitters(long maxAge) {
	//int sizeBefore = this.size();
    	this.entrySet().removeIf(entry -> System.currentTimeMillis() - entry.getValue().getLastHeard() > maxAge);
	/*int sizeAfter = this.size();
	if (sizeAfter != sizeBefore) {
            System.out.println("Rimossi " + (sizeBefore - sizeAfter) + " zitters");
	}*/
    }

    public String prettyPrint() {
        StringBuilder sb=new StringBuilder();
        for(Zitter z: values()) {
            sb.append(z.toString());
            sb.append("\n");
        }
        sb.append("[");
        sb.append(size());
        sb.append("]");
        return sb.toString();
    }

}
