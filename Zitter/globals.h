#include <Arduino.h>

// stato del nodo
extern String nomeNodo;
extern uint16 canaleCorrente;  // NOTE: si sappia che non abbiamo modo di sapere LCN rispetto al canale memorizzato sul TV
extern boolean mute; // (e data dell'ultimo cambio)
//extern uint8 volume; // (e data dell'ultimo cambio)
extern boolean tvOn;
extern boolean presenzaPubblicita;
extern uint8 gradimento; // 0-5

String getStatusString();
String getStatusStringHTML();

String testoScrolling="";
int posizioneScroll=0;


#define AP_NAME "zitter-ap."
#define AP_PASS ""


#if defined(ARDUINO_ARCH_ESP8266)
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#define WIFI_EVENT_STA_CONNECTED      WIFI_EVENT_STAMODE_CONNECTED
#define WIFI_EVENT_STA_DISCONNECTED   WIFI_EVENT_STAMODE_DISCONNECTED
#define WIFI_EVENT_AP_STACONNECTED    WIFI_EVENT_SOFTAPMODE_STACONNECTED
#define WIFI_EVENT_AP_STADISCONNECTED WIFI_EVENT_SOFTAPMODE_STADISCONNECTED
#define WIFI_EVENT_ALL                WIFI_EVENT_ANY
#define WIFI_AUTH_OPEN                ENC_TYPE_NONE
#elif defined(ARDUINO_ARCH_ESP32)
#include <WiFi.h>
#include <WebServer.h>
#include <SPIFFS.h>
#define WIFI_EVENT_STA_CONNECTED      SYSTEM_EVENT_STA_CONNECTED
#define WIFI_EVENT_STA_DISCONNECTED   SYSTEM_EVENT_STA_DISCONNECTED
#define WIFI_EVENT_AP_STACONNECTED    SYSTEM_EVENT_AP_STACONNECTED
#define WIFI_EVENT_AP_STADISCONNECTED SYSTEM_EVENT_AP_STADISCONNECTED
#define WIFI_EVENT_ALL                SYSTEM_EVENT_MAX
#endif
