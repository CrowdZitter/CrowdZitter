#!/bin/bash

##########################################################
IDNODE=$(echo $0|cut -f2 -d'-'|cut -f1 -d.)
echo NODE: $IDNODE
##########################################################
. ./ZitterSim.sh
##########################################################

canale 20
sleep 2

volume 4
sleep 2

mute true
sleep 2

rating 5
sleep 2

advert true
sleep 2

tvOn true
