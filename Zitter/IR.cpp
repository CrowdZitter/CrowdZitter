// infrarossi

#include <IRremoteESP8266.h>
#include <IRrecv.h>
#include <IRsend.h>

#include "IR.h"
#include "Config.h"
#include "globals.h"
#include "Network.h"

#include "TM1638-manager.h"
#include "pinconfig.h"

#define TRAIN_TIMEOUT 1000 // intervals

// TODO implementare modo di salvare e recuperare il training (estrazione da SPIFFS?)

uint16_t CAPTURE_BUFFER_SIZE = 1024;
uint16_t RECV_PIN = IRRX;
uint16_t SEND_PIN = IRTX;

typedef struct {
    String etichetta;
    uint64_t scancode;
} Tasto;

IRsend irsend(SEND_PIN);
IRrecv irrecv(RECV_PIN, CAPTURE_BUFFER_SIZE, TIMEOUT_MS, true);
decode_results results; // contiene anche il tipo

Tasto tasti[]= {
    {"0",0},
    {"1",0},
    {"2",0},
    {"3",0},
    {"4",0},
    {"5",0},
    {"6",0},
    {"7",0},
    {"8",0},
    {"9",0},
    {"mute",0},
    {"volume+",0},
    {"volume-",0},
    {"prog+",0},
    {"prog-",0},
    {"off",0}
};
int dimTasti=(sizeof(tasti)/sizeof(Tasto));
int getDimTasti() {
    return dimTasti;
}

int mapTasto(String etichetta) {
    for(int i=0; i<dimTasti; i++) {
        if(tasti[i].etichetta==etichetta) return i;
    }
    return 0;
}

uint64_t scancodeTasto(String etichetta) {
    Serial.print(F("scancodeTasto: "));
    Serial.println((unsigned long)tasti[mapTasto(etichetta)].scancode);
    return tasti[mapTasto(etichetta)].scancode;
}


/**
 * returns true if NOT expired
 */
boolean scanSingle() {
    irrecv.resume();

    int timeout=0;

    // recv+decode bloccante
    while (
        !irrecv.decode(&results)
        &&
        timeout < TRAIN_TIMEOUT
    ) {
        delay(10);
        timeout++;
    }

    return (timeout < TRAIN_TIMEOUT);
}

// TODO LOW verificare lettura e saving del decode type
void train() {
    Serial.println(F("training..."));
    for(int i=0; i<dimTasti; i++) {
        Serial.println(tasti[i].etichetta);

        setLcdToString(tasti[i].etichetta);

        if(!scanSingle()) return; // exit WITHOUT save

        // fill
        tasti[i].scancode=results.value;

        Serial.println((unsigned long)results.value);

        setLcdToString(String((unsigned long)results.value));

        delay(500);
        irrecv.resume();
    }

    Serial.print(F("decode type: "));
    Serial.println(results.decode_type);

    Serial.println(F("training complete"));

    setLcdToString("train complete");

    saveConfig();
}

void list() {
    Serial.println(F("/ tasti \\"));
    for(int i=0; i<dimTasti; i++) {
        Serial.print(tasti[i].etichetta);
        Serial.print(F(": "));
        Serial.println((unsigned long)tasti[i].scancode);
    }
    Serial.print(F("tipo: "));
    Serial.println(results.decode_type);
    Serial.println(F("\\ tasti /"));
}

void ir_setup() {
    irsend.begin();
    irrecv.enableIRIn();
}

void irSend(String s) {
    Serial.println(F("irSend"));
    irsend.send(results.decode_type,scancodeTasto(s),getBits());

    /*
    Serial.println("invio IR di prova");
    irsend.send(RC5,scancodeTasto(s),64);
    */
}

uint64_t getScancode(int i) {
    return tasti[i].scancode;
}

void setScancode(int i, uint64_t scan) {
    tasti[i].scancode=scan;
}

decode_type_t getDecodetype() {
    return results.decode_type;
}


void setDecodetype(decode_type_t dtype) {
    results.decode_type=dtype;
}

void setBits(uint16_t bits) {
    results.bits=bits;
}


uint16_t getBits() {
    return results.bits;
}



void chanPlus() {
    canaleCorrente++;
    //displayValue(CHAN);
    irSend("prog+");
    mqttSendChan();
}

void chanMinus() {
    canaleCorrente--;
    //displayValue(CHAN);
    irSend("prog-");
    mqttSendChan();
}

void volMinus() {
    //volume--;
    //displayValue(VOLDOWN);
    irSend("volume-");
    mqttSendVolumeDown();
}

void volPlus() {
    //volume++;
    //displayValue(VOLUP);
    irSend("volume+");
    mqttSendVolumeUp();
}

void toggleMute() {
    mute = !mute;
    //nextValueToDisplay=MUTE;
    irSend("mute");
    mqttSendMute();
}

void toggleTV() {
    tvOn = !tvOn;
    //nextValueToDisplay=ON;
    if(!tvOn) irSend("off");
    mqttSendTv();
}

void togglePUB() {
    Serial.println(F("*** PUB ***"));
    presenzaPubblicita = !presenzaPubblicita;
    //displayValue(PUB);
    mqttSendPub();
}


void sendChan(String ch) {
// siamo già quasi sicuri che è un numero per atoi

    int l=ch.length();
    for(int i=0; i<l; i++) {
        irSend(ch.substring(i,i+1));
        delay(20); //TODO verificare se lasciarlo o se cambiare valore
    }
}
