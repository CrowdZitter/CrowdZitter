#include <Arduino.h>

boolean wifi_setup();
void mqtt_reconnect();
void mqtt_send(String topic,String payload);
void rxMQTT(char* topic, byte* payload, unsigned int length);
void mqtt_setup();
void mqttSendInt(String topic, int value);
void mqttSendBool(String topic, boolean value);
void mqttSendPub();
void mqttSendMute();
void mqttSendTv();
void mqttSendVolumeUp();
void mqttSendVolumeDown();
void mqttSendChan();
void mqttLoop();
void ImAlive();
//void consumeOSC();
