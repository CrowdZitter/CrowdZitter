# Procedura di learning (praticamente DONE)

- decidiamo di farlo funzionare in "learning" per non dover prevedere settemila telecomandi
- da "ui" selezionare "learning"
- tasti: 0-9, mute, volume+-, prog+-, off

una struttura che memorizza il tipo del telecomando

```
// formato file config
TIPO TELECOMANDO
0
1
2
3
4
5
6
7
8
9
mute
volume+
volume-
prog+
prog-
off
```

# MQTT

Tipi di dato:
- scancode
- canale
- inizio pubblicità
- fine pubblicità
- orario
- ...

tutti pubblicano su MQTT

uno o più "motori decisionali" ascoltano, calcolano, pubblicano risultato della "votazione"

tutti ascoltano i messaggi dell'oracolo (motore decisionale)

TODO rifare schema lavagna

## Messaggi

CrowdZitter/<idNode>/[status|oracle]/[chan, volume, mute, pub, anime, sf, ...] <value>

Espansione dell'ultima parte del topic:

channel [number]
mute [true/...]
volume [number]
tvOn [true/...]
advertising [true/...]
rating [number]

### Esempi

CrowdZitter/567/segnalazione/canaleCorrente	5
CrowdZitter/567/segnalazione/mute	true
CrowdZitter/567/segnalazione/volume	5
CrowdZitter/567/segnalazione/tvon	true
CrowdZitter/567/segnalazione/pub	true
CrowdZitter/567/segnalazione/gradimento	5


## IDEA DI STATUS periodico/su evento (cambio stato)

- canale corrente
- mute (e data dell'ultimo cambio)
- volume valore (e data dell'ultimo cambio)
- tv on/off
- pub si/no/data ultimo cambio
- gradimento



# Use case del Nodo

## Prima configurazione

- imposta wifi
- imposta MQTT
- learning
- ...

## Configurazioni varie (extends Prima configurazione)

- configura azioni (a fronte di un messaggio ricevuto)

## Uso passivo (usa Prima configurazione)

- setta un canale
- attende eventi dalla coda MQTT
- scatena azioni configurate

## Uso attivo (extends Uso passivo)

- setta e cambia canale ad libitum
- manda stato nodo (su evento es. cambio canale)


# TM1638

*	*	*	*	*	*	*	*

8	8	8	8	8	8	8	8

[]	[]	[]	[]	[]	[]	[]	[]


## contesto default

chan-	chan+	vol-	vol+	mute(on/off) 	pub(on/off)		tv(on/off)	menu

## contesto config/reset/sync

???	???	???	???	???	???	???	menu

## contesto <altro>

???	???	???	???	???	???	???	menu



# OSC app per Android

## Criteri

- file config dell'interfaccia su app deve essere facilmente esportabile/importabile
- file config facilmente editabile

AndrOSC è rimasto forse ultimo disponibile licenziato libero


## per catturare pacchetti OSC (debug) da PC

nc -l -u -k 7400 

(keep è importante quando si usa indirizzo broadcast)
(attenzione al firewall!!!)



# bottoni che ci servono sul telecomando Android


