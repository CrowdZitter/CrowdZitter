import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.*;

/**
 * rappresenta uno zitter remoto (è un proxy)
 */
public class Zitter {
    private String nomeNodo;

    public String getName() {
        return nomeNodo;
    };

    private int channel;
    private boolean mute;
    private int volume;
    private boolean tvOn;
    private boolean advertising;
    private int rating; // 0-5
    private long lastHeard;

    public int getChannel() {
        return channel;
    }

    public long getLastHeard() {
    	return lastHeard;
    }

    private void setChannel(int value) {
        channel = value;
        advertising = false;
    }

    private void setMute(boolean value) {
        mute = value;
    }

    private void setVolume(int value) {
        volume = value;
    }

    private void setTvOn(boolean value) {
        tvOn = value;
    }

    private void setAdvertising(boolean value) {
        advertising = value;
    }

    private void setRating(int value) {
        rating = value;
    }

    public ThreeStateAnswer advertOn(int ch) {
        if(ch != channel) return ThreeStateAnswer.IDONTKNOW;
        return advertising ? ThreeStateAnswer.YES : ThreeStateAnswer.NO;
    }

    public String toString() {
        // TODO LOW localizzazione stringhe?
        StringBuilder sb=new StringBuilder();
        sb.append("(");
        sb.append(super.toString());
        sb.append(", name=");
        sb.append(nomeNodo);
        sb.append(", channel=");
        sb.append(channel);
        sb.append(", vol=");
        sb.append(volume);
        sb.append(", mute=");
        sb.append(mute);
        sb.append(", tvOn=");
        sb.append(tvOn);
        sb.append(", advert=");
        sb.append(advertising);
        sb.append(", liking=");
        sb.append(rating);
        sb.append(")");
        return sb.toString();
    };

    /**
     * qui dovrebbe arrivare solo la parte rimanente del topic
     */
    public void absorbMessage(String restOfTopic, MqttMessage message) {
        lastHeard=System.currentTimeMillis();

        setTvOn(true); // per non ripeterlo in tutti i set

        //StringTokenizer st=new StringTokenizer(restOfTopic,Globals.TOPIC_DELIMITER);

        //System.err.println("Zitter.absorbing: "+restOfTopic+", "+message);

        // bruttarello, passare ad un meccanismo ad hash o addirittura a reflection?

        try {
            switch(restOfTopic) {
            case Globals.CHANNEL_SUBTOPIC:
                //System.out.println("*** CANALE: " + message);
                setChannel(Integer.parseInt(message.toString()));
                break;
            case Globals.VOL_SUBTOPIC:
                setVolume(Integer.parseInt(message.toString()));
                break;
            case Globals.MUTE_SUBTOPIC:
                setMute(Boolean.parseBoolean(message.toString()));
                break;
            case Globals.TVON_SUBTOPIC:
                setTvOn(Boolean.parseBoolean(message.toString()));
                break;
            case Globals.ADVERT_SUBTOPIC:
                setAdvertising(Boolean.parseBoolean(message.toString()));
                break;
            case Globals.RATING_SUBTOPIC:
                setRating(Integer.parseInt(message.toString()));
                break;
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public Zitter(String name) {
        nomeNodo=name;
    }
}
