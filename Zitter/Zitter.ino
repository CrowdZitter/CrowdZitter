/**
    Zitter (nodo)

  	Copyright 2018 Elio Monza, Gianluca Nitti, Andrea Trentini

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// DONE gestione interfaccia web per impostazione wifi
// TODO interazione usando https://github.com/Hieromon/PageBuilder/blob/master/examples/FSPage/README.md

//#include <ArduinoOTA.h> // TODO se ci dovesse servire lo riabilitiamo
#include "pinconfig.h"
#include "globals.h"

// stato del nodo
String nomeNodo=String(ESP.getChipId());
uint16 canaleCorrente;  // NOTE: si sappia che non abbiamo modo di sapere LCN rispetto al canale memorizzato sul TV
boolean mute; // (e data dell'ultimo cambio)
//uint8 volume; // (e data dell'ultimo cambio)
boolean tvOn;
boolean presenzaPubblicita;
uint8 gradimento; // 0-5

String getStatusString() {
    String status=String("Ch:");
    status+=String(canaleCorrente);

    //status+=", MUTE:";
    status+=(mute ? String("MUTE ON") : String("MUTE OFF"));
    /*
    status+=", VOL:";
    status+=String(volume);
    */
    status+=", PUB:";
    status+=String(presenzaPubblicita);
    status+=", TV:";
    status+=String(tvOn);
    status+=", MQTT:";
    status+=String(false); // TODO status mqtt
    return status;
}
String getStatusStringHTML() {
    String status=String("Ch:");
    status+=String(canaleCorrente);
    //status+="<br>MUTE:";
    status+=(mute ? String("<br>MUTE ON") : String("<br>MUTE OFF"));
    /*
    status+="<br>VOL:";
    status+=String(volume);
    */
    status+="<br>PUB:";
    status+=String(presenzaPubblicita);
    status+="<br>TV:";
    status+=String(tvOn);
    status+="<br>MQTT:";
    status+=String("TODO status MQTT"); // TODO status mqtt
    return status;
}


#include "Network.h"

void irSend(String s);

// Tastiera
#include "TM1638-manager.h"

#include "IR.h"

#include "Config.h"

#include "WebUI.h"

#include <TaskScheduler.h>
Scheduler runner;
void showStatus();
Task taskShowStatus(TASK_SECOND, TASK_FOREVER, &showStatus);
Task taskScroller(100, TASK_FOREVER, &scrollDisplay);
Task taskReact(100, TASK_FOREVER, &react); // legge bottoni e reagisce (trasmette MQTT)
Task taskMQTTloop(TASK_SECOND, TASK_FOREVER, &mqttLoop);
Task taskImAlive(30*TASK_SECOND, TASK_FOREVER, &ImAlive);
//Task taskOSC(100, TASK_FOREVER, &consumeOSC);


//////////////////////////////////////////////////////////////////
void setup() {
    Serial.begin(115200);
    Serial.println(F("Booting..."));
    setLcdToString("BOOT...");

    fs_setup();
    ir_setup();

    readConfig();
    list();

    setLcdToString("WIFI");
    wifi_setup(); // TODO ripensare posizionamento?
    webui_setup();

    setLcdToString("MQTT");
    mqtt_setup();

    delay(3000);

    // Tasks
    runner.init();

    runner.addTask(taskShowStatus);
    taskShowStatus.enable();

    runner.addTask(taskScroller);
    taskScroller.enable();

    runner.addTask(taskReact);
    taskReact.enable();

    runner.addTask(taskMQTTloop);
    taskMQTTloop.enable();

    runner.addTask(taskImAlive);
    taskImAlive.enable();

    /*
    runner.addTask(taskOSC);
    taskOSC.enable();
    */

    // display
    setLcdToString("OK");
    Serial.println(F("setup complete"));
}


/**
 - leggere tasti
 - cambiare stato di conseguenza
 - inviare IR codes in funzione dei tasti
 - inviare MQTT in funzione dei tasti/stato
 - eventualmente entrare in learning (su combinazione tasti?)
 - mqtt.loop gestisce callback in ricezione
*/
void loop() {
    // runner task
    runner.execute();

    // OTA
    //ArduinoOTA.handle();

    webui_loop(); // TODO eventualmente renderlo un task
}

/* TODO LOW parametrizzare/config
 * - password OTA
 * - indirizzo server MQTT
 * - ...
 */

// TODO unificazione di tutti i file O trovare meccanismo nativo di Arduino
