#!/bin/bash

##########################################################
### simula uno zitter, mettere in fondo il proprio "script"

MQTTSERVER="atrent.it"

##########################################################
ROOT_TOPIC="CrowdZitter"
TOPIC_DELIMITER="/"

SIGNAL_SUBTOPIC="status"
ORACLE_SUBTOPIC="oracle"

CHANNEL_SUBTOPIC="channel"
MUTE_SUBTOPIC="mute"
VOL_SUBTOPIC="volume"
TVON_SUBTOPIC="tvOn"
ADVERT_SUBTOPIC="advertising"
RATING_SUBTOPIC="rating"

### Messaggi
#    CrowdZitter/<idNode>/[status|oracle]/[chan, volume, mute, pub, anime, sf, ...] <value>
#    Espansione dell'ultima parte del topic:
#    channel [number]
#    mute [true/...]
#    volume [number]
#    tvOn [true/...]
#    advertising [true/...]
#    rating [number]
#
#### Esempi
#
#    CrowdZitter/567/segnalazione/canaleCorrente	5
#    CrowdZitter/567/segnalazione/mute	true
#    CrowdZitter/567/segnalazione/volume	5
#    CrowdZitter/567/segnalazione/tvon	true
#    CrowdZitter/567/segnalazione/pub	true
#    CrowdZitter/567/segnalazione/gradimento	5

mqttSend(){
	#1 segnal/oracolo
	#2 tipo
	#3 valore
	mosquitto_pub -h $MQTTSERVER -t ${ROOT_TOPIC}${TOPIC_DELIMITER}${IDNODE}${TOPIC_DELIMITER}${1}${TOPIC_DELIMITER}${2} -m ${3}
}

mqttSendSegnalazione(){
	#1 tipo
	#2 valore
	mqttSend ${SIGNAL_SUBTOPIC} ${1} ${2}
}

canale(){
	mqttSendSegnalazione ${CHANNEL_SUBTOPIC} ${1}	
}

volume(){
	mqttSendSegnalazione ${VOL_SUBTOPIC} ${1}	
}

mute(){
	mqttSendSegnalazione ${MUTE_SUBTOPIC} ${1}	
}

rating(){
	mqttSendSegnalazione ${RATING_SUBTOPIC} ${1}	
}

advert(){
	mqttSendSegnalazione ${ADVERT_SUBTOPIC} ${1}	
}

tvOn(){
	mqttSendSegnalazione ${TVON_SUBTOPIC} ${1}	
}
