/** WiFi & OTA & NTP & MQTT
*/

//#include <OSCBundle.h>
//#include <OSCBoards.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

/*
const byte MAX_MSG_SIZE PROGMEM=100;
byte packetBuffer[MAX_MSG_SIZE];  //buffer to hold incoming udp packet
long oscLastPacketTime=0;
WiFiUDP Udp;
*/

#include "globals.h"
#include "pinconfig.h"

#include "Network.h"

// TODO LOW config wifi via web/app/osc/etc.

WiFiClient espClient;
IPAddress gateway;
byte mac[6];

#define ROOT_TOPIC "CrowdZitter/"
// la lib è PubSubClient (https://pubsubclient.knolleary.net/)
#include <PubSubClient.h>  // mqtt
PubSubClient mqtt_client(espClient);
#define DELAY_MQTT 1000
String mqtt_server = "atrent.it";
long lastMsg = 0;
#define MSG_LEN 150
char msg[MSG_LEN];

// nel file wifiauth (NON gittato per ovvi motivi) mettere def di ssid e pass come sotto
//#include "wifiauth.h"
//String ssid = "...";
//String password = "...";


#define WIFI_TENTATIVI 30
#define USE_GW "[gw]"

// NTP
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP,+60*60); // UTC+1

// WiFi event determination and echo back.
void broadcastEvent(WiFiEvent_t event) {
    static const char eventText_APSTA_CONN[] PROGMEM = "SoftAP station connected.";
    static const char eventText_APSTA_DISC[] PROGMEM = "SoftAP station disconnected.";
    static const char eventText_STA_CONN[] PROGMEM = "WiFi AP connected.";
    static const char eventText_STA_DISC[] PROGMEM = "WiFi AP disconnected.";
    const char* eventText;

    switch (event) {
    case WIFI_EVENT_AP_STACONNECTED:
        eventText = eventText_APSTA_CONN;
        break;
    case WIFI_EVENT_AP_STADISCONNECTED:
        eventText = eventText_APSTA_DISC;
        break;
    case WIFI_EVENT_STA_CONNECTED:
        eventText = eventText_STA_CONN;
        break;
    case WIFI_EVENT_STA_DISCONNECTED:
        eventText = eventText_STA_DISC;
        break;
    default:
        eventText = nullptr;
    }
    if (eventText)
        Serial.println(String("[event] ") + String(FPSTR(eventText)));
}



/** setup all */
boolean wifi_setup() {
    delay(500);
    /* TODO riordinare sequenza definendo "automa" delle varie possibilità

    	bootWiFi (non connesso, non si sa se wifi memorizzato)
    	|
    	> tenta connessione STA (con timeout)
    	|         |
    	|=Fail    |=OK
    	|         |
    	|         connesso a AP
        |
    	|
    	AttivaSoftAP (webui)

    */

    // bootWiFi
    //WiFi.mode(WIFI_STA); //sembra cancellare le credenziali salvate
    Serial.print(F("=== Connecting to: "));
    //Serial.println(ssid);
    Serial.println(WiFi.SSID());
    //WiFi.disconnect();
    WiFi.begin();
    //WiFi.begin(ssid.c_str(), password.c_str());

    for (int i=0; (WiFi.status() != WL_CONNECTED) && (i < WIFI_TENTATIVI) && digitalRead(0)==HIGH; i++) {
        delay(500);
        Serial.print(F("."));
    }

    // AttivaSoftAP
    if(WiFi.status() != WL_CONNECTED) {
        // Start WiFi soft AP.
        WiFi.softAPdisconnect();
        //WiFi.disconnect();
        delay(100);
        WiFi.mode(WIFI_AP_STA);
        String apname=AP_NAME+String(ESP.getChipId());
        WiFi.softAP(apname.c_str() /*, AP_PASS*/);
        while (WiFi.softAPIP() == IPAddress(0, 0, 0, 0)) { //blocca finchè l'AP è partito
            yield();
            delay(100);
        }
        Serial.print(AP_NAME " started. IP:");
        Serial.println(WiFi.softAPIP());
    }

    // Turn on WiFi event handling.
    WiFi.onEvent(broadcastEvent, WIFI_EVENT_ALL);

    if(WiFi.status() == WL_CONNECTED) {
        Serial.println();
        Serial.println(F("+++ WiFi connected!"));

        Serial.print(F("IP: "));
        Serial.println(WiFi.localIP());
        testoScrolling=WiFi.localIP().toString();

        Serial.print(F("GW: "));
        Serial.println(WiFi.gatewayIP());

        delay(500);

        timeClient.begin();
        timeClient.update(); // TODO andrebbe invocato periodicamente (in un Task)

        /*
        // Port defaults to 8266
        ArduinoOTA.setPort(8266);

        // Hostname defaults to esp8266-[ChipID]
        // ArduinoOTA.setHostname("myesp8266");

        ArduinoOTA.setPassword((const char *)"987654321");

        ArduinoOTA.onStart([]() {
            Serial.println(F("OTA Start"));
        });
        ArduinoOTA.onEnd([]() {
            Serial.println(F("OTA End"));
        });
        ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
            Serial.printf("OTA Progress: %u%%\r", (progress / (total / 100)));
        });
        ArduinoOTA.onError([](ota_error_t error) {
            Serial.printf("OTA Error[%u]: ", error);
            if (error == OTA_AUTH_ERROR) Serial.println(F("Auth Failed"));
            else if (error == OTA_BEGIN_ERROR) Serial.println(F("Begin Failed"));
            else if (error == OTA_CONNECT_ERROR) Serial.println(F("Connect Failed"));
            else if (error == OTA_RECEIVE_ERROR) Serial.println(F("Receive Failed"));
            else if (error == OTA_END_ERROR) Serial.println(F("End Failed"));
        });

        ArduinoOTA.begin();
        Serial.println(F("+++ OTA Ready"));

        //setup ethernet part
        Udp.begin(7400);
        */

        return true;
    } else {
        Serial.println(F("--- WiFi FAILED!"));
        return false;
    }
}


void mqtt_reconnect() {
    // TODO LOW ripulire
    // TODO LOW nr. tentativi???  per forza perche' se non trova mqtt server non fa piu' nulla

    // Loop until we're reconnected
    if (!mqtt_client.connected()) {
        Serial.print(F("Attempting MQTT connection..."));
        // Attempt to connect
        if (mqtt_client.connect(nomeNodo.c_str())) {  // si connette al BROKER dichiarando "identità"
            Serial.println(F("connected"));

            // Once connected, publish an announcement...
            mqtt_client.publish(nomeNodo.c_str(), "first msg.");

            // ... and resubscribe
            Serial.print(F("subscribing: "));
            Serial.println(mqtt_client.subscribe("CrowdZitter/#"));

        } else {
            Serial.print(F("failed, rc="));
            Serial.print(mqtt_client.state());
            Serial.println(F(" skipping MQTT..."));
            // Wait 5 seconds before retrying
            //delay(5000);
        }
    }
}

void mqtt_send(String topic,String payload) {
    if (WiFi.status() == WL_CONNECTED) {
        if (!mqtt_client.connected()) {
            mqtt_reconnect();
            Serial.println(F("reconnected..."));
        }

        if (mqtt_client.connected()) {
            //mqtt_client.loop();

            if (mqtt_client.publish(topic.c_str(), payload.c_str())) {
                Serial.println(F("info: MQTT message succesfully published"));
            } else {
                Serial.println(F("error: MQTT publishing error (connection error or message too large)"));
            }
        }
    }
}

void rxMQTT(char* topic, byte* payload, unsigned int length) {
    Serial.print(F("Topic: "));
    Serial.println(topic);

    Serial.print(F("Payload: "));
    for(int i=0; i<length; i++) {
        Serial.print((char)payload[i]);
    }

    Serial.print(F("\nLength: "));
    Serial.println(length);
}

void mqtt_setup() {
    mqtt_client.setServer(mqtt_server.c_str(), 1883); // 1883 è porta default MQTT
    mqtt_client.setCallback(rxMQTT);
    mqtt_reconnect();
    Serial.println(F("mqtt set"));

}

/* TODO LOW definire stringhe standard con define?
   (tipo makefile o simili che da un master ESTERNO
   in documentazione genera un include per lo zitter e/o un .java per il nostro oracolo
*/

void mqttSendInt(String topic, int value) {
    mqtt_send(ROOT_TOPIC + nomeNodo + "/status/" + topic, String(value));
    delay(REACT_DELAY);
}

void mqttSendBool(String topic, boolean value) {
    mqtt_send(ROOT_TOPIC + nomeNodo + "/status/" + topic, value ? "true" : "false");
    delay(REACT_DELAY);
}

void mqttSendPub() {
    mqttSendBool("advertising", presenzaPubblicita);
}

void mqttSendMute() {
    mqttSendBool("mute", mute);
}

void mqttSendTv() {
    mqttSendBool("tvOn", tvOn);
}

void mqttSendVolumeUp() {
    mqttSendInt("volume", +1);
}

void mqttSendVolumeDown() {
    mqttSendInt("volume", -1);
}

void mqttSendChan() {
    mqttSendInt("channel", canaleCorrente);
}

// task che manda un heartbeat
void ImAlive() {
    mqttSendInt("heap", ESP.getFreeHeap());
}

// TODO LOW implementare sendgradimento (cfr. anche implementare gradimento in TM1638...)

// Task che ascolta MQTT
void mqttLoop() {
    mqtt_client.loop();
}

// TODO LOW cercare se possibile lanciare un interrupt su ricezione pacchetto

// TODO LOW spostare OSC in un file a parte?


// TODO cambiarlo nel gestore dei topic per telecomando
/*
void accel(OSCMessage &msg, int addrOffset) {
    Serial.println("=========================");
    Serial.println(msg.getFloat(0));
    //Serial.println(msg.getFloat(1));
    //Serial.println(msg.getFloat(2));
    Serial.println("=========================");
}
*/

/*
void consumeOSC() {
    //Serial.print(F("+"));

    //OSCBundle bundleIN; // NO
    OSCMessage messageIN;
    int size;
    if( (size = Udp.parsePacket())>0) {
        oscLastPacketTime=millis();

        Serial.println(F("received"));
        Serial.printf("Received %d bytes from %s, port %d\n", size, Udp.remoteIP().toString().c_str(), Udp.remotePort());

        Udp.read(packetBuffer,size);
        messageIN.fill(packetBuffer,size);

        // TODO attenzione al formato OSC che in varie app sembra diverso
        // es.  /accelerometer valore
        // es.  /accelerometer/65.0


        if(!messageIN.hasError()) {
            //Serial.println("----------->  no error");
            //messageIN.route("/temperature", temp);
            //messageIN.route("/magnetometer", magn);
            //messageIN.route("/accelerometer", accel);
            messageIN.route("/accelerometer", accel);
        }
        Udp.flush();
    }
}
*/
