#!/bin/bash

rm *class

#astyle -A2 *java

#export CLASSPATH=$CLASSPATH:~/apps/paho.mqtt.java/org.eclipse.paho.client.mqttv3/target/org.eclipse.paho.client.mqttv3-1.2.0.jar
export CLASSPATH=$CLASSPATH:./org.eclipse.paho.client.mqttv3-1.2.0.jar

javadoc -d docs -package *java

if
 javac Oracolo.java
then
 java Oracolo $(hostname)
fi

exit
if
 ! kotlinc -cp $CLASSPATH *kt
then
 exit 2
fi
