public class Globals {

    // TODO: definire elenco costanti per tutti i messaggi, soprattutto MQTT che devono essere standardizzati e fissati per sempre o quasi

    public static final String ROOT_TOPIC="CrowdZitter";

    public static final String TOPIC_DELIMITER="/";

    public static final String SIGNAL_SUBTOPIC="status";
    public static final String ORACLE_SUBTOPIC="oracle";

    public static final String CHANNEL_SUBTOPIC="channel";
    public static final String MUTE_SUBTOPIC="mute";
    public static final String VOL_SUBTOPIC="volume";
    public static final String TVON_SUBTOPIC="tvOn";
    public static final String ADVERT_SUBTOPIC="advertising";
    public static final String RATING_SUBTOPIC="rating";

    public static final String ADVERT_ON_CHANNEL="advertisingOnChannel";
    public static final String NO_ADVERT_ON_CHANNEL="noAdvertisingOnChannel";

    /*
    ### Messaggi

    CrowdZitter/<idNode>/[status|oracle]/[chan, volume, mute, pub, anime, sf, ...] <value>

    Espansione dell'ultima parte del topic:

    channel [number]
    mute [true/...]
    volume [number]
    tvOn [true/...]
    advertising [true/...]
    rating [number]

    #### Esempi

    CrowdZitter/567/segnalazione/canaleCorrente	5
    CrowdZitter/567/segnalazione/mute	true
    CrowdZitter/567/segnalazione/volume	5
    CrowdZitter/567/segnalazione/tvon	true
    CrowdZitter/567/segnalazione/pub	true
    CrowdZitter/567/segnalazione/gradimento	5
    */
}
