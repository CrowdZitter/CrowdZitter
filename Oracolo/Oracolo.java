// TODO inserire GPL

// TODO prep statistiche/status sensato per demone

import java.util.*;
import java.io.*;


// VEDERE NoteImplementazione.md per definizione topic standard

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.*;

public class Oracolo implements MqttCallback {
    public static void main(String[] args)
    throws Exception {
        Oracolo o=new Oracolo(args[0]);
        System.out.println(o);
        o.run();
    }

    public Oracolo(String id) {
        this.id=id;
    }

    private String id;

    private  Zitters zitters=new Zitters();

    private MqttClient subscriber,publisher; // splittato per non avere overlapping di chiamate in callback

    @Override
    public void connectionLost(Throwable cause) {
        System.err.println("*** connection lost!");
        System.err.println(cause);
    }


    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        System.err.println("*** delivery complete!");
        System.err.println(token);

    }

    @Override
    public void messageArrived(String topic, MqttMessage message)
    //throws Exception
    {
        System.out.print("===> Topic: "+topic);
        System.out.println(" - Message: "+message);

        /*
        System.out.println("/// Zitters prima: ");
        System.out.println(zitters.prettyPrint());
        System.out.println("Sono: "+zitters.size());
        System.out.println("///");
        */

        Zitter z=zitters.absorbMessage(topic,message);
        System.out.println("Zitter affected: "+z);

        if(z!=null) {
            int channel=z.getChannel();
            //if(zitters.size()%10==0)
            oracleAdvertise(channel);
        }

        //System.out.println("/// Zitters dopo: ");
        //System.out.println(zitters.prettyPrint());
        System.out.print("Sono: "+zitters.size());
        //System.out.println("///");

        //mqttConnect();
    }


    public void oracleAdvertise(int channel) {
        long howmany=zitters.howManyAdvertise(channel);

        System.out.println("+++ howmanyadvertise on channel "+channel+": "+howmany);

        // migliorabile, sia come "algoritmo", sia come stringhe MQTT (vedi Globals.java)
        if(howmany >= 2)
            sendMessage(Globals.ADVERT_ON_CHANNEL, Integer.toString(channel));
        else
            sendMessage(Globals.NO_ADVERT_ON_CHANNEL, Integer.toString(channel));
    }

    public void sendMessage(String tipo, String messaggio) {
        //   CrowdZitter/7676/oracolo/<tipo>
        try {
            publisher.publish(
                Globals.ROOT_TOPIC +
                //"ORACLE"+
                Globals.TOPIC_DELIMITER +
                id +
                Globals.TOPIC_DELIMITER +
                Globals.ORACLE_SUBTOPIC +
                Globals.TOPIC_DELIMITER +
                tipo, new MqttMessage(messaggio.getBytes()));
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }


    public void mqttConnect() throws Exception {

        // Nota Bene: con una singola connessione si "impallava", mah!
        publisher = new MqttClient("tcp://atrent.it", id+"-pub");
        publisher.connect();
        subscriber = new MqttClient("tcp://atrent.it", id+"-sub");
        subscriber.connect();

        subscriber.setCallback(this);
        subscriber.subscribe(
            Globals.ROOT_TOPIC+
            Globals.TOPIC_DELIMITER+
            "+"+ // idnode
            Globals.TOPIC_DELIMITER+
            Globals.SIGNAL_SUBTOPIC+  // status
            Globals.TOPIC_DELIMITER+
            "#");
    }

    public void run()
    throws Exception {
        // test 1 zitter
        /*
        Zitter z=new Zitter("567");
        zitters.put(z);
        System.out.println(zitters);
        */


        // test mqtt
        /*
        String topic        = Globals.ROOT_TOPIC;
        String content      = "Message from MqttPublishSample";
        int qos             = 0;
        //String broker       = "tcp://iot.eclipse.org:1883";
        String broker       = "tcp://atrent.it:1883";
        String clientId     = "JavaSample";
        MemoryPersistence persistence = new MemoryPersistence();
        */

        mqttConnect();
        /*
        MqttMessage message = new MqttMessage();
        message.setPayload("A single message from my computer fff"
                           .getBytes());
        client.publish("foo", message);
        */

        /*
        try {
            MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            System.out.println("Connecting to broker: "+broker);
            sampleClient.connect(connOpts);
            System.out.println("Connected");
            System.out.println("Publishing message: "+content);
            MqttMessage message = new MqttMessage(content.getBytes());
            message.setQos(qos);
            sampleClient.publish(topic, message);
            System.out.println("Message published");
            sampleClient.disconnect();
            System.out.println("Disconnected");
            System.exit(0);
        } catch(MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
            me.printStackTrace();
        }
        */
    }
}
