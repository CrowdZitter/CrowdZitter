// https://github.com/Hieromon/PageBuilder

#include "PageBuilder.h"

String listSSID(PageArgument& args);
unsigned int toWiFiQuality(int32_t rssi);
String reqConnect(PageArgument& args);
void connWiFi();
String resConnect(PageArgument& args);
void broadcastEvent(WiFiEvent_t event);
String getArch(PageArgument& args);

void webui_loop();
void webui_setup();
