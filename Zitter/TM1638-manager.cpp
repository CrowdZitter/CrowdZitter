//#include <TM1638.h>
#include <TM1638lite.h>

#include "TM1638-manager.h"
#include "Network.h"
#include <ESP8266WiFi.h>
#include "IR.h"
#include "pinconfig.h"
#include "globals.h"

// define a module on data pin 8, clock pin 9 and strobe pin 7
TM1638lite module(TMSTROBE, TMCLOCK, TMDATA);

byte keys;
boolean menuMain=true;
unsigned long lastPress;
#define DISPLAY_TIMEOUT 60000

// TODO LOW rendere più responsive tastierino e web, migliorare display 7seg

// NOTE attenzione che alcuni gpio inibiscono wifi

// NOTE con la nuova lib manca ad es. il display off (verificare standby/powersave)

// NOTE la disposizione sulla keyboard è invertita
// TODO LOW LOW gestire nomenclatura costanti (es. menu diversi)
//chan-	chan+	vol-	vol+	mute(on/off) 	pub(on/off)		tv(on/off)	menu
#define BUTTON_CHANM B01000000
#define BUTTON_CHANP B10000000
#define BUTTON_VOLM  B00010000
#define BUTTON_VOLP  B00100000
#define BUTTON_MUTE  B00001000
#define BUTTON_PUB   B00000100
#define BUTTON_TV    B00000010
#define BUTTON_MENU  B00000001

#define BRIGHT 2  // TODO LOW LOW LOW diventerà un variabile pilotata da una fotocellula/orario

// cosa visualizzare sul display come prossimo item
#define CHAN 0
#define PUB 1
#define IP 2

#define SUBMENU 99

int nextValueToDisplay=0;
String status;

/*
void TM1638_test() {
    // display a hexadecimal number and set the left 4 dots
    //module.setDisplayToHexNumber(0x1234ABCD, 0xF0);
    byte keys = module.readButtons();
    // light the first 4 red LEDs and the last 4 green LEDs as the buttons are pressed
    setLEDs(((keys & 0xF0) << 8) | (keys & 0xF));
    setLEDs(255);
    module.setDisplayToString("ELIOMAXI",100);
}
*/

void setLEDs(byte b) {
    for (int i = 0; i < 7; i++) {
        module.setLED(i, bitRead(b, i));
    }
}

boolean isButton(byte button) {
    return ((keys & button) > 0);
}

void reactSub() {
    nextValueToDisplay=SUBMENU;
    train();
}

void displayValue(int v) {
    nextValueToDisplay=v;
    showStatus();
}

/**
 - getbuttons
 - sync stato
 - invia IR ???
 - invia MQTT
*/
void reactMain() {
    if(isButton(BUTTON_VOLM)) {
        volMinus();
    }

    if(isButton(BUTTON_VOLP)) {
        volPlus();
    }

    if(isButton(BUTTON_MUTE)) {
        toggleMute();
    }

    // TODO LOW ripensare stato on off del tv
    if(isButton(BUTTON_TV)) {
        toggleTV();
    }

    if(isButton(BUTTON_PUB)) {
        togglePUB();
        displayValue(PUB);
    }

// TODO LOW LOW implementare gradimento
//uint8 gradimento; // 0-5

    if(isButton(BUTTON_CHANP) && canaleCorrente < 255) {
        chanPlus();
        displayValue(CHAN);
    }

    if(isButton(BUTTON_CHANM) && canaleCorrente > 0) {
        chanMinus();
        displayValue(CHAN);
    }
}

void scrollDisplay(){
	module.displayText(testoScrolling.substring(posizioneScroll));
	posizioneScroll++;
	if(posizioneScroll>testoScrolling.length()) posizioneScroll=0;
}

void update7seg() {
    setLEDs(0);

    //rssi
    int rssi=map(WiFi.RSSI(),-100,-30,1,8); // TODO LOW LOW LOW check questi range

    /*
    Serial.print(F("rssi: "));
    Serial.println(rssi);
    */

    setLEDs(~(0xFF << rssi));    
    module.displayText(status);
}

String pad(int value) {
    String v=String(value);
    String final;
    for(int i=v.length(); i<4; i++)
        final.concat(" ");
    final.concat(v);
    return final;
}

// mostra un valore ad ogni invocazione
void showStatus() {
    switch(nextValueToDisplay) {
    case CHAN:
        status="Chan";
        status.concat(pad(canaleCorrente));
        break;

    /* TODO LOW pensare a come visualizzare la pressione del tasto v+ e v- che non hanno più uno stato sottoforma di variabile
    case VOLDOWN:
        status="Vol DOWN";
        //status.concat(pad(volume));
        break;

    case VOLUP:
        status="Vol   UP";
        //status.concat(pad(volume));
        break;
    */

    case SUBMENU:
        status="Train... ";
        break;

    case PUB:
        status="Pub ";
        if(presenzaPubblicita)
            status.concat("  Si");
        else
            status.concat("  No");
        break;

    case IP:
        status=WiFi.localIP().toString();
        break;

//String nomeNodo=String(ESP.getChipId());
//uint8 gradimento; // 0-5
    }
    nextValueToDisplay++;
    if(nextValueToDisplay>2) // NOTE aggiornare quando si mostrano più valori dello status, TODO passare a enum?
        nextValueToDisplay=0;

    update7seg();

    /*
    Serial.println(status);
    Serial.print(F("Heap: "));
    Serial.println(ESP.getFreeHeap());
    */
}

void react() {
    // se c'è roba sulla seriale, nel formato ssid,password allora la usa
    if(Serial.available()) {
        String ssid=Serial.readStringUntil(',');
        Serial.println(ssid);
        String password=Serial.readStringUntil('\n');
        Serial.println(password);
        WiFi.begin(ssid.c_str(), password.c_str());
        wifi_setup();
    }


    keys=module.readButtons();

    // questo accende il LED corrispondente al tasto premuto
    if(keys!=0) {
        setLEDs(keys);
        lastPress=millis();
    }

    if(isButton(BUTTON_MENU))
        menuMain=!menuMain;
    else {
        if(menuMain) {
            reactMain();
            //Serial.println(F("main "));
        } else {
            reactSub();
            menuMain=!menuMain;
            //Serial.println(F("sub"));
        }
    }
}

void setLcdToString(String s) {
    //module.clearDisplay();
    module.displayText(s);
}





// TODO visualizzare IP sul display
