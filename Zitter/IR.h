#include <IRrecv.h>

int mapTasto(String etichetta);
uint64_t scancodeTasto(String etichetta);
//void scanSingle();
void train();
void list();
void ir_setup();
void irSend(String s);
int getDimTasti();
uint64_t getScancode(int i);
void setScancode(int i, uint64_t scan);
decode_type_t getDecodetype();
void setDecodetype(decode_type_t dtype);
void setBits(uint16_t bits);
uint16_t getBits();
void chanPlus();
void chanMinus();
void volMinus();
void volPlus();
void toggleMute();
void toggleTV();
void togglePUB();
void sendChan(String ch);
