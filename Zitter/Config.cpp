#include <Arduino.h>
#include <FS.h>

#include "Config.h"
#include "IR.h"

#define FILENAME "/telecomando"

void saveConfig() {
    Serial.println(F("saving..."));

    // open file for writing
    File f = SPIFFS.open(FILENAME, "w");

    if (!f) {
        Serial.println(F("file write failed"));
        return;
    }

    // convert&write
    char stringa[50];
    for(int i=0; i<getDimTasti(); i++) {
        ////f.println(itoa(tasti[i].scancode,stringa,16));
        //f.write((uint8_t*)&(tasti[i].scancode),sizeof(tasti[i].scancode));
        uint64_t scan=getScancode(i);
        f.write((uint8_t*)&(scan),sizeof(scan));
    }

    //f.write((uint8_t*)&(results.decode_type),sizeof(results.decode_type));
    decode_type_t dtype=getDecodetype();
    f.write((uint8_t*)&(dtype),sizeof(dtype));

    uint16_t bits=getBits();
    f.write((uint8_t*)&(bits),sizeof(bits));


    f.close();
    f.flush();
    Serial.println(F("saving complete"));
}

void readConfig() {
    File f = SPIFFS.open(FILENAME, "r");

    if (!f) {
        Serial.println(F("file read failed"));
        return;
    }

    for(int i=0; i<getDimTasti(); i++) {
        /*
        Serial.print(F("READCONFIG... "));
        Serial.print(tasti[i].etichetta);
        Serial.print(F(": "));
        */
        uint64_t scan;
        f.read((uint8_t*)&(scan),sizeof(scan));
        setScancode(i,scan);
        //Serial.println((unsigned long)tasti[i].scancode);
    }

    //f.read((uint8_t*)&(results.decode_type),sizeof(results.decode_type));
    decode_type_t dtype;
    f.read((uint8_t*)&(dtype),sizeof(dtype));
    setDecodetype(dtype);

    uint16_t bits;
    f.read((uint8_t*)&(bits),sizeof(bits));
    setBits(bits);
}

void fs_setup() {
    SPIFFS.begin();
}
