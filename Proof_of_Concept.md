# USE CASES
	- Utente attivo/passivo
		- Passivo (RICEZIONE):
			installa apparecchio
			configurazione
		- Attivo (SEGNALAZIONE):
			PASSIVO + schiaccia bottoni

	- "Motore" validazione segnalazioni
		validatore segnalazioni con reputation
		(Blockchain? Ethereum?)

# APPARECCHIO
	- BASE
		Trasmettitore telecomando infrarosso
		per pilotare il televisore
		(Wemos/RasPi + IR + interfaccia utente)
	- ADVANCE
		APPARECCHIO BASE + ricevitore IR

# ESTENSIONI
	- Crowd auditel(DB eventi)
	- Azioni programmabili
		- cambio canale
		- luci
		- musica
		- thè, caffè
		- mail
		- ...
	- Valutazioni (Tastino Like/Dislike)
	- Sostituzione telecomando
	- MQTT pubblico? (tipo Twitter)




# Bozza schema hardware

+ Wemos
+ TM1638 (keyboard e display)
+ LED IR TX
+ (opzionale) OLED


## Allocazione piedini (Versione BASE)

3 per TM1638
2 per OLED
1 per LED IR
